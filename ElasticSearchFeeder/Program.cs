﻿using System;
using System.Diagnostics;
using System.Fabric;
using System.Threading;
using Microsoft.Diagnostics.EventListeners;
using Microsoft.Diagnostics.EventListeners.Fabric;

namespace ElasticSearchFeeder
{
    internal class Program
    {
        /// <summary>
        /// This is the entry point of the service host process.
        /// </summary>
        private static void Main()
        {
            try
            {
                // Creating a FabricRuntime connects this host process to the Service Fabric runtime.
                using (FabricRuntime fabricRuntime = FabricRuntime.Create())
                {
                    // **** Instantiate ElasticSearchListener
                    var configProvider = new FabricConfigurationProvider("ElasticSearchEventListener");
                    ElasticSearchListener esListener = null;
                    if (configProvider.HasConfiguration)
                    {
                        esListener = new ElasticSearchListener(configProvider, new FabricHealthReporter("FeederApplication"));
                    }
                    fabricRuntime.RegisterServiceType("ElasticSearchFeederType", typeof(ElasticSearchFeeder));

                    ServiceEventSource.Current.ServiceTypeRegistered(Process.GetCurrentProcess().Id, typeof(ElasticSearchFeeder).Name);
                    

                    Thread.Sleep(Timeout.Infinite);  // Prevents this host process from terminating so services keeps running.
                    GC.KeepAlive(esListener);
                }
            }
            catch (Exception e)
            {
                ServiceEventSource.Current.ServiceHostInitializationFailed(e.ToString());
                throw;
            }
        }
    }
}
